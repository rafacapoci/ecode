create database ecode;

use ecode;

create table users (
	id int primary key auto_increment,
    nome varchar(100) not null,
    dataNascimento date not null,
    email varchar(100) not null,
    password varchar(240) not null,
    cpf varchar(20) not null,
    rg varchar(20) not null,
    sexo varchar(20) not null,
    cep varchar(20) not null,
    endereco varchar (100) not null,
    numero int not null,
    bairro varchar(100) not null,
    complemento varchar(100),
    estado char(2) not null,
    cidade varchar(100) not null,
    telefone varchar(20),
    celular varchar(20) not null,
    situacao boolean,
    tipoUsuario varchar(20) not null
);