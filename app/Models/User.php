<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    public $timestamps = false;
    use HasFactory, Notifiable;

    protected $fillable = [
        'nome',
        'dataNascimento',
        'email',
        'password',
        'cpf',
        'rg',
        'sexo',
        'cep',
        'endereco',
        'numero',
        'bairro',
        'complemento',
        'estado',
        'cidade',
        'telefone',
        'celular',
        'situacao',
        'tipoUsuario'
    ];
    
    protected $hidden = [
        'password',
        'remember_token',
    ];

    
    
}
