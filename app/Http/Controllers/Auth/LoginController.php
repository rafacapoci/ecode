<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Pessoa;

class LoginController extends Controller {

    //Autenticação
    use AuthenticatesUsers;
    protected $redirectTo = RouteServiceProvider::HOME;
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    //Metodos
    public function indexView() {
        return view('login.index');
    }


}
