<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller {
    
    use RegistersUsers;

    //Redireciona o usuario após o cadastro
    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct() {
        $this->middleware('guest');
    }

    //Validando dados chave
    protected function validator(array $data) {
        return Validator::make($data, [
            'nome' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'dataNascimento' => ['required'],
            'sexo' => ['required'],
            'cpf' => ['required'],
            'rg' => ['required'],
            'celular' => ['required', 'min:8'],
            'situacao' => ['required'],
            'tipoUsuario' => ['required'],
            'cep' => ['required'],
            'endereco' => ['required'],
            'numero' => ['required'],
            'bairro' => ['required'],
            'complemento' => ['required'],
            'cidade' => ['required'],
            'estado' => ['required'],
        ]);
    }

    //Inserindo no banco de dados
    protected function create(array $data) {
        return User::create([
            'nome' => $data['nome'],
            'dataNascimento' => $data['dataNascimento'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'cpf' => $data['cpf'],
            'rg' => $data['rg'],
            'sexo' => $data['sexo'],
            'cep' => $data['cep'],
            'endereco' => $data['endereco'],
            'numero' => $data['numero'],
            'bairro' => $data['bairro'],
            'complemento' => $data['complemento'],
            'estado' => $data['estado'],
            'cidade' => $data['cidade'],
            'telefone' => $data['telefone'],
            'celular' => $data['celular'],
            'situacao' => $data['situacao'],
            'tipoUsuario' => $data['tipoUsuario']
        ]);

    }
}
