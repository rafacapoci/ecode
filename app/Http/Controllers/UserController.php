<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class UserController extends Controller {

    //Cliente
    public function indexView() {
        return view('users.index');
    }

    public function index(){
        $users = User::all();
        return $users->toJson();
    }

    //Cadastro 
    public function store(Request $request) {
        $novoUser = new User();
        $novoUser->nome = $request->input('nome');
        $novoUser->dataNascimento = $request->input('dataNascimento');
        $novoUser->email = $request->input('email');
        $novoUser->password = Hash::make($request->input('password'));
        $novoUser->cpf = $request->input('cpf');
        $novoUser->rg = $request->input('rg');
        $novoUser->sexo = $request->input('sexo');
        $novoUser->cep = $request->input('cep');
        $novoUser->endereco = $request->input('endereco');
        $novoUser->numero = $request->input('numero');
        $novoUser->bairro = $request->input('bairro');
        $novoUser->complemento = $request->input('complemento');
        $novoUser->estado = $request->input('estado');
        $novoUser->cidade = $request->input('cidade');
        $novoUser->telefone = $request->input('telefone');
        $novoUser->celular = $request->input('celular');
        $novoUser->situacao = $request->input('situacao');
        $novoUser->tipoUsuario = $request->input('tipoUsuario');;
        $novoUser->save();

        return json_encode($novoUser);
    }


    //Mostra dados
    public function show($id) {
        $ShowUser = User::find($id);
        if (isset($ShowUser)) {
            return json_encode($ShowUser);
        }
    }

    //Atualiza 
    public function update(Request $request, $id) {
        $updateUser = User::find($id);

        $updateUser->nome = $request->input('nome');
        $updateUser->dataNascimento = $request->input('dataNascimento');
        $updateUser->email = $request->input('email');
        if ($request->input('password') != '') {
            $updateUser->password = Hash::make($request->input('password'));            
        }
        $updateUser->cpf = $request->input('cpf');
        $updateUser->rg = $request->input('rg');
        $updateUser->sexo = $request->input('sexo');
        $updateUser->cep = $request->input('cep');
        $updateUser->endereco = $request->input('endereco');
        $updateUser->numero = $request->input('numero');
        $updateUser->bairro = $request->input('bairro');
        $updateUser->complemento = $request->input('complemento');
        $updateUser->estado = $request->input('estado');
        $updateUser->cidade = $request->input('cidade');
        $updateUser->telefone = $request->input('telefone');
        $updateUser->celular = $request->input('celular');
        $updateUser->situacao = $request->input('situacao');
        $updateUser->tipoUsuario = $request->input('tipoUsuario');
        $updateUser->save();
        
        return json_encode($updateUser);
    }

    //Deleta uma Etapa
    public function destroy($id) {
        $DeleteUser = User::find($id);
        if (isset($DeleteUser)) {
            $DeleteUser->delete();
        }
    }
}
