<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Pesquisa;

use Illuminate\Http\Request;

class PesquisaController extends Controller {

    public function indexView() {
        return view('pesquisa.index');
    }
    
    public function index(Request $request){
        //$pesquisa = User::where($request->input('tipoPesquisa'),  $request->input('conteudoPesquisa'));
        $pesquisa = User::where($request->input('tipoPesquisa'),  $request->input('conteudoPesquisa'));
        return json_encode($pesquisa);
    }

    
    public function update(Request $request, $id) {
        $updateUser = User::find($id);

        $updateUser->nome = $request->input('nome');
        $updateUser->dataNascimento = $request->input('dataNascimento');
        $updateUser->email = $request->input('email');
        $updateUser->password = Hash::make($request->input('password'));
        $updateUser->cpf = $request->input('cpf');
        $updateUser->rg = $request->input('rg');
        $updateUser->sexo = $request->input('sexo');
        $updateUser->cep = $request->input('cep');
        $updateUser->endereco = $request->input('endereco');
        $updateUser->numero = $request->input('numero');
        $updateUser->bairro = $request->input('bairro');
        $updateUser->complemento = $request->input('complemento');
        $updateUser->estado = $request->input('estado');
        $updateUser->cidade = $request->input('cidade');
        $updateUser->telefone = $request->input('telefone');
        $updateUser->celular = $request->input('celular');
        $updateUser->situacao = $request->input('situacao');
        $updateUser->tipoUsuario = $request->input('tipoUsuario');
        $updateUser->save();
        
        return json_encode($updateUser);
    }

    //Deleta uma Etapa
    public function destroy($id) {
        $DeleteUser = User::find($id);
        if (isset($DeleteUser)) {
            $DeleteUser->delete();
        }
    }
}
