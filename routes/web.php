<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PesquisaController;

use Illuminate\Support\Facades\Route;

//Abre direto na pagina de login
Route::get('/', [App\Http\Controllers\HomeController::class, 'indexView']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'indexView']);

Route::get('/users', [UserController::class, 'indexView']);
Route::get('/pesquisa', [PesquisaController::class, 'indexView']);
Auth::routes();

