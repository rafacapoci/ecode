<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>@yield('titulo')</title>
    
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" 
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" 
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>   

<!--Inicio Container -->
<div class="container-fluid">
<!-- -------------------------------------------------------------------------------------------------------------- -->
    <!--Inicio Linha UM -->
    <div class="row m-4">
        <!--Inicio coluna UM -->
        <div class="col-auto">
            <!--Inicio barra de menu (navbar) -->
            <nav class="navbar-light navbar-expand-md">
                <!--Botão colaps-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" 
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span>@yield('nomeMenu')</span>
                </button>

                <!--Inicio dos botões -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <a type="button" class="btn btn-primary m-1 text-light" href="/home">Pagina inical</a>
                        <a type="button" class="btn btn-outline-primary m-1" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Sair
                        </a>            
                    </ul>                
                </div>
                <!--Fim dos botões -->
            </nav>
            <!--Fim barra de menu (navbar) -->
        </div>
        <!--Fim coluna 1 (um) -->
    </div>
    <!--Fim Linha 1 (um) -->

<!-- -------------------------------------------------------------------------------------------------------------- -->

    <!--Inicio Linha 2 (dois/LISTAGEM) -->
    <div class="row justify-content-center m-4">
        <!--Inicio coluna 2 (dois) -->
        <div class="col-12 mb-5">
            <p class="h2 mt-3 mb-2 text-center">
                @yield('tituloLista')
                @yield('barraPesquisa')
            </p>
            <!--Inicio Lista -->
            <table class="table table-hover table-bordered mb-5" id="@yield('idTabela')">
                <thead class="text-center">
                    @yield('cabecalhoLista')
                </thead>
                <tbody>
                    
                </tbody> 
            </table>
            <!--Fim Lista -->
        </div>
        <!--Fim coluna 2 (dois) -->
    </div>
    <!--Fim Linha 2 (dois/LISTAGEM) -->

<!-- -------------------------------------------------------------------------------------------------------------- -->

    <!--Inicio Linha Modal Add -->
    <div class="row fixed-bottom p-4 justify-content-right">
        <!--Inicio coluna Modal-->
        <div class="col-12 text-right">

<!-- -------------------------------------------------------------------------------------------------------------- -->

            <!--Inicio botão para abrir Modal-->
            <button type="button" @yield('adm') class="btn btn-lg btn-primary shadow-lg" data-toggle="modal" data-target="#modalAdd" onclick="@yield('zerarInputs')">
                @yield('btnAdd')
            </button>
            <!--Fim botão para abrir Modal-->

<!-- -------------------------------------------------------------------------------------------------------------- -->

        </div>
        <!--Fim coluna Modal-->
    </div>
    <!--Fim Linha Modal Add -->

<!-- -------------------------------------------------------------------------------------------------------------- -->

    <!--Inicio Form Modal Adicionar-->
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            
            <div class="modal-content">

                <div class="modal-header justify-content-center">
                    <h5 class="modal-title font-weight-bold" id="exampleModalLabel">@yield('tituloModal')</h5>
                </div>

                <form  method="post" id="formularioAdd">
                    @csrf

                    <div class="modal-body">
                            
                        @yield('formularioAdicionar')
                            
                    </div>

                    <div class="modal-footer">
                        <button type="cancel" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Salvar</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
    <!--Fim Form Modal Adicionar-->

</div>
<!--Fim Container -->

</body>
</html>

<script>

    $(function(){

        //Função para gerar token, necessario para metodos post----------------------------------------
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        //Função para cadastrar novo item (STORE)------------------------------------------------------
        $("#formularioAdd").submit(
        function(event){
            event.preventDefault();

            if ($("#id").val() != '') {
                @yield('nomeNovoItem') = {
                    id: $('#id').val(),
                    @yield('conteudoNovoItem')
                };

                $.ajax({
                    type: "PUT",
                    url: "/api/@yield('getpostjson')/" + @yield('nomeNovoItem').id,
                    context: this,
                    data: @yield('nomeNovoItem'),
                    success: function(data){
                        atualizarItem = JSON.parse(data);
                        console.log(atualizarItem);

                        linhas = $('#@yield('idTabela')>tbody>tr');

                        linhaAtualizar = linhas.filter(function(i, elemento) {
                                return elemento.cells[0].textContent == atualizarItem.id;
                        });
                        if (linhaAtualizar) {
                            @yield('linhaAtualizar')
                        }
                    },
                    error: function(error){
                        console.log(error);
                    }
                });

            } else {
                @yield('nomeNovoItem') = {
                    @yield('conteudoNovoItem')
                };
                $.post("/api/@yield('getpostjson')", @yield('nomeNovoItem'), function(data){
                    
                    novoItem = JSON.parse(data);
                    console.log(novoItem);
                    var linha =  @yield('atualizarTabela')
                        '<td class="text-center">' + 
                            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
                            '<button type="cancel" class="btn btn-danger" onclick="excluir('+ novoItem.id +')">Excluir</button>' +
                        '</td>' +
                    '</tr>';
                    $('#@yield('idTabela') > tbody').append(linha);
                })
            }

            $("#modalAdd").modal('hide');

            @yield('zerarInputs');
        });

        //Função para carregar lista de itens cadastrados (INDEX)--------------------------------------
        $.getJSON('/api/@yield('getpostjson')', function(@yield('getpostjson')){
            console.log(@yield('getpostjson'));
            if (@yield('repetir')) {
                for (i = 0; i < @yield('getpostjson').length; i++){
                    
                    var linha = @yield('corpoLista');
                    
                    $('#@yield('idTabela') > tbody').append(linha);
                    
                }
            } else {
                for (i = 0; i < 1; i++){
                    var linha = @yield('corpoLista');
                    
                    $('#@yield('idTabela') > tbody').append(linha);
                    
                }
            }

        });

    });

    
    //Função para excluir item -----------------------------------------------------------------------
    function excluir(id){
        //Confirmar exclusão
        var deletar = confirm("Tem certeza que deseja excluir esta conta?");
        if (deletar == true) {
            $.ajax({
                type: "DELETE",
                url: "/api/@yield('getpostjson')/" + id,
                context: this,
                success: function(){
                    linhas = $('#@yield('idTabela')>tbody>tr');
                    linhaDeletar = linhas.filter(
                        function(i, elemento) {
                        return elemento.cells[0].textContent == id;
                    });

                    if (linhaDeletar) {
                        linhaDeletar.remove();
                    }
                },
                error: function(error){
                    console.log(error);
                }
            });
        } else {
            alert("Exclusão cancelada!")
        }
        
    }

    //Função para pegar dados a editar do item-----------------------------------------------------------------------------
    function editar(id) {
        $.getJSON('/api/@yield('getpostjson')/'+id, function(data){
            @yield('carregarDadosEditar')
        });
    }

    
</script>

    