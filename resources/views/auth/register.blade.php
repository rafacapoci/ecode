<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Criar conta</title>
    @extends('layouts.bootstrap') 
</head>
<body>

    <div class="container">
        <h2 class="text-center">Novo Cadastro</h2>
        
        <!--Inicio linha-->
        <form action="{{ route('register') }}"  method="post" id="formularioAdd">
            @csrf
            <div class="row justify-content-center mt-3 mb-3">

                <!--Inicio coluna-->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h3 class="text-center">Dados pessoais</h3>
                    <input hidden type="text" class="form-control" name="id" id="id">
                    <div class="form-group">
                        <label for="nome">Nome *</label>
                        <input type="text" class="form-control @error('nome') is-invalid @enderror" name="nome" id="nome" placeholder="Nome completo">
                        @error('nome')
                            <span class="invalid-feedback" role="alert">
                                <strong>Coloque um nome.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <label for="dataNascimento">Data de nascimento *</label>
                                <input type="date" class="form-control @error('dataNascimento') is-invalid @enderror" name="dataNascimento" id="dataNascimento">
                                @error('dataNascimento')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Coloque sua data de nascimento.</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="sexo">Sexo *</label>
                                <select name="sexo" id="sexo" class="form-select form-control @error('sexo') is-invalid @enderror">
                                    <option selected></option>
                                    <option value="masculino">Masculino</option>
                                    <option value="feminino">Feminino</option>
                                    <option value="outro">Outros</option>
                                </select>
                                @error('sexo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Informe sua sexualidade.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <label for="cpf">CPF *</label>
                                <input type="text" class="form-control @error('cpf') is-invalid @enderror" name="cpf" id="cpf" onkeypress="$(this).mask('000.000.000-00');" placeholder="CPF para cadastro">
                                @error('cpf')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Coloque seu CPF.</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="rg">RG *</label>
                                <input type="text" class="form-control @error('rg') is-invalid @enderror" name="rg" id="rg" onkeypress="$(this).mask('00000000-0');" placeholder="RG para cadastro">
                                @error('rg')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Coloque seu RG.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail *</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="E-mail também sera seu usuario">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>Informe um e-mail valido.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Senha *</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="senha para novo usuario">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>Informe uma senha valida.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <label for="telefone">Telefone</label>
                                <input type="text" class="form-control" onkeypress="$(this).mask('(00) 0000-00009')" name="telefone" id="telefone" placeholder="Telefone fixo caso possua">
                            </div>
                            <div class="col-6">
                                <label for="celular">Celular *</label>
                                <input type="text" class="form-control @error('celular') is-invalid @enderror" onkeypress="$(this).mask('(00) 0 0000-0000')"  name="celular" id="celular" placeholder="Celular principal">
                                @error('celular')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Coloque um numero de celular.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="situacao">Situação *</label>
                                <select name="situacao" id="situacao" class="form-select form-control @error('situacao') is-invalid @enderror">
                                    <option selected></option>
                                    <option value="1">Ativo</option>
                                    <option value="0">Inativo</option>
                                </select>
                                @error('situacao')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Coloque a situação do cliente.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="tipoUsuario">Tipo de cliente *</label>
                                <select name="tipoUsuario" id="tipoUsuario" class="form-select form-control @error('tipoUsuario') is-invalid @enderror">
                                    <option selected></option>
                                    <option value="Cliente">Normal</option>
                                    <option value="Administrador(a)">Administrador</option>
                                </select>
                                @error('tipoUsuario')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Coloque o tipo do usuario.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h3 class="text-center">Endereço</h3>
                    <div class="form-group">
                        <label for="cep">CEP *</label>
                        <input type="text" class="form-control @error('cep') is-invalid @enderror" onkeypress="$(this).mask('00.000-000')" name="cep" id="cep" placeholder="CEP">
                        @error('cep')
                            <span class="invalid-feedback" role="alert">
                                <strong>Coloque seu CEP.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="endereco">Endereço *</label>
                        <input type="text" class="form-control @error('endereco') is-invalid @enderror" name="endereco" id="endereco" placeholder="Rua">
                        @error('endereco')
                            <span class="invalid-feedback" role="alert">
                                <strong>Coloque seu endereço.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="numero">Numero *</label>
                        <input type="text" class="form-control @error('numero') is-invalid @enderror" name="numero" id="numero" placeholder="Numero casa ou apartamento">
                        @error('numero')
                            <span class="invalid-feedback" role="alert">
                                <strong>Coloque o numero da sua casa/apartamento.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="bairro">Bairro *</label>
                        <input type="bairro" class="form-control @error('bairro') is-invalid @enderror" name="bairro" id="bairro" placeholder="Bairro">
                        @error('bairro')
                            <span class="invalid-feedback" role="alert">
                                <strong>Coloque seu bairro.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="complemento">Complemento *</label>
                        <input type="text" class="form-control @error('complemento') is-invalid @enderror" name="complemento" id="complemento" placeholder="Local de referencia">
                        @error('complemento')
                            <span class="invalid-feedback" role="alert">
                                <strong>Coloque algum complemento.</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <label for="cidade">Cidade *</label>
                                <input type="text" class="form-control @error('cidade') is-invalid @enderror" name="cidade" id="cidade" placeholder="Cidade">
                                @error('cidade')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Coloque sua cidade.</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="estado">Estado *</label>
                                <select name="estado" id="estado" class="form-select form-control @error('estado') is-invalid @enderror">
                                    <option selected></option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                                @error('estado')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Coloque seu estado.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 text-right">
                    <a href="/" class="btn btn-danger" data-dismiss="modal">Cancelar</a>
                </div>
                <div class="col-6 text-left">
                    <button type="submit" class="btn btn-success">Cadastrar</button>
                </div>
            </div>
                
        </form>
    </div>
    
</body>
</html>
