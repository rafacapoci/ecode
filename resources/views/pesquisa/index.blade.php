@extends('layouts.principal')
<!--Formulario para logout-->
<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</form>
<!--Titulo-->
@section('titulo', 'Pesquisando por ' . $_GET['conteudoPesquisa'])
<!--Titulo Listagem e barra pesquisar-->
    @section('tituloLista', 'Pesquisando por ' . $_GET['conteudoPesquisa'])
    @section('barraPesquisa')
        <form action="/pesquisa" method="GET">
            <div class="input-group">
                <select class="form-control col-2 rounded-left" name="tipoPesquisa" id="tipoPesquisa">
                    <option value="nome">Nome</option>
                    <option value="sexo">Sexo</option>
                    <option value="estado">Estado</option>
                </select>
                <input type="text" name="conteudoPesquisa" id="conteudoPesquisa" class="form-control rounded-right mr-3" placeholder="Digite aqui..."/>
                <button type="submit" class="btn btn-outline-primary">Pesquisar</button>
            </div>                
        </form>
    @endsection
<!--Inicio Tabela de Listagem-->
@section('idTabela', 'pesquisa')
@section('cabecalhoLista')
    <th>Nome</th>
    <th>Email</th>
    <th>Cidade</th> 
    <th>Ações</th> 
@endsection
@section('corpoLista')
    @section('getpostjson', 'pesquisa')

    @if(auth()->user()->tipoUsuario == 'Administrador(a)')
    @section('repetir', 1)
    '<tr>' +
        '<td hidden>' + pesquisa[i].id + '</td>' +
        '<td>' + pesquisa[i].nome + '</td>' +
        '<td>' + pesquisa[i].email + '</td>' +
        '<td>' + pesquisa[i].cidade +'</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ pesquisa[i].id +')">Editar</button>' +
            '<button type="cancel" class="btn btn-danger" onclick="excluir('+ pesquisa[i].id +')">Deletar conta</button>' +
        '</td>' +
    '</tr>'
    @endif
    
@endsection
<!--Botoes Modal Adicionar Cargo-->
    @section('adm', 'hidden')
    @section('desativado', 'disabled')
    @section('formularioAdicionar')
    <input hidden type="text" class="form-control" name="id" id="id">
    <div class="form-group">
        <label for="nome">Nome *</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome completo">
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                <label for="dataNascimento">Data de nascimento *</label>
                <input type="date" class="form-control" name="dataNascimento" id="dataNascimento">
            </div>
            <div class="col-6">
                <label for="sexo">Sexo *</label>
                <select name="sexo" id="sexo" class="form-select form-control">
                    <option selected></option>
                    <option value="masculino">Masculino</option>
                    <option value="feminino">Feminino</option>
                    <option value="outro">Outros</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                <label for="cpf">CPF *</label>
                <input type="text" class="form-control" name="cpf" id="cpf" placeholder="CPF para cadastro">
            </div>
            <div class="col-6">
                <label for="rg">RG *</label>
                <input type="text" class="form-control" name="rg" id="rg" placeholder="RG para cadastro">
            </div>
        </div>                        
    </div>
    <div class="form-group">
        <label for="email">E-mail *</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail também sera seu usuario">
    </div>
    <div class="form-group">
        <label for="password">Senha *</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="senha para novo usuario">
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                <label for="telefone">Telefone</label>
                <input type="text" class="form-control" name="telefone" id="telefone" placeholder="Telefone fixo caso possua">
            </div>
            <div class="col-6">
                <label for="celular">Celular *</label>
                <input type="text" class="form-control" name="celular" id="celular" placeholder="Celular principal">
            </div>
        </div>                        
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="situacao">Situação *</label>
                <select name="situacao" id="situacao" class="form-select form-control">
                    <option selected></option>
                    <option value="1">Ativo</option>
                    <option value="0">Inativo</option>
                </select>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label for="tipoUsuario">Tipo de cliente *</label>
                <select name="tipoUsuario" id="tipoUsuario" class="form-select form-control" @yield('desativado')>
                    <option selected></option>
                    <option value="Cliente">Cliente</option>
                    <option value="Administrador(a)">Administrador</option>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <h3 class="text-center">Endereço</h3>
    <div class="form-group">
        <label for="cep">CEP *</label>
        <input type="text" class="form-control" name="cep" id="cep" placeholder="CEP">
    </div>
    <div class="form-group">
        <label for="endereco">Endereço *</label>
        <input type="text" class="form-control" name="endereco" id="endereco" placeholder="Rua">
    </div>
    <div class="form-group">
        <label for="numero">Numero *</label>
        <input type="text" class="form-control" name="numero" id="numero" placeholder="Numero casa ou apartamento">
    </div>
    <div class="form-group">
        <label for="bairro">Bairro *</label>
        <input type="bairro" class="form-control" name="bairro" id="bairro" placeholder="Bairro">
    </div>
    <div class="form-group">
        <label for="complemento">Complemento *</label>
        <input type="text" class="form-control" name="complemento" id="complemento" placeholder="Local de referencia">
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-6">
                <label for="cidade">Cidade *</label>
                <input type="text" class="form-control" name="cidade" id="cidade" placeholder="Cidade">
            </div>
            <div class="col-6">
                <label for="estado">Estado *</label>
                <select name="estado" id="estado" class="form-select form-control">
                    <option selected></option>
                    <option value="AC">Acre</option>
                    <option value="AL">Alagoas</option>
                    <option value="AP">Amapá</option>
                    <option value="AM">Amazonas</option>
                    <option value="BA">Bahia</option>
                    <option value="CE">Ceará</option>
                    <option value="DF">Distrito Federal</option>
                    <option value="ES">Espírito Santo</option>
                    <option value="GO">Goiás</option>
                    <option value="MA">Maranhão</option>
                    <option value="MT">Mato Grosso</option>
                    <option value="MS">Mato Grosso do Sul</option>
                    <option value="MG">Minas Gerais</option>
                    <option value="PA">Pará</option>
                    <option value="PB">Paraíba</option>
                    <option value="PR">Paraná</option>
                    <option value="PE">Pernambuco</option>
                    <option value="PI">Piauí</option>
                    <option value="RJ">Rio de Janeiro</option>
                    <option value="RN">Rio Grande do Norte</option>
                    <option value="RS">Rio Grande do Sul</option>
                    <option value="RO">Rondônia</option>
                    <option value="RR">Roraima</option>
                    <option value="SC">Santa Catarina</option>
                    <option value="SP">São Paulo</option>
                    <option value="SE">Sergipe</option>
                    <option value="TO">Tocantins</option>
                </select>
            </div>
        </div>
    </div>
@endsection
<!--Javascript Adicionar Cargo-->
@section('nomeNovoItem', 'novoUser')
@section('atualizarTabela')

    '<tr>' +
        '<td hidden>' + novoItem.id + '</td>' +
        '<td>' + novoItem.nome + '</td>' +
        '<td>' + novoItem.email + '</td>' +
        '<td>' + novoItem.cidade + '</td>' +
        '<td class="text-center">' + 
            '<button type="button" class="btn btn-primary mr-1" onclick="editar('+ novoItem.id +')">Editar</button>' +
            '<button type="cancel" class="btn btn-danger" onclick="excluir('+ novoItem.id +')">Deletar conta</button>' +
        '</td>' +
    '</tr>'

@endsection
<!--Javascript Zerar inputs-->
@section('zerarInputs')
    $('#id').val(''),
    $('#nome').val(''),
    $('#dataNascimento').val(''),
    $('#email').val(''),
    $('#password').val(''),
    $('#cpf').val(''),
    $('#rg').val(''),
    $('#sexo').val(''),
    $('#cep').val(''),
    $('#endereco').val(''),
    $('#numero').val(''),
    $('#bairro').val(''),
    $('#complemento').val(''),
    $('#estado').val(''),
    $('#cidade').val(''),
    $('#telefone').val(''),
    $('#celular').val(''),
    $('#situacao').val(''),
    $('#tipoUsuario').val('')
@endsection
<!--Javascript Editar Etapa-->
@section('carregarDadosEditar')
    $('#id').val(data.id),
    $('#nome').val(data.nome),
    $('#dataNascimento').val(data.dataNascimento),
    $('#email').val(data.email),
    $('#password').val(data.password),
    $('#cpf').val(data.cpf),
    $('#rg').val(data.rg),
    $('#sexo').val(data.sexo),
    $('#cep').val(data.cep),
    $('#endereco').val(data.endereco),
    $('#numero').val(data.numero),
    $('#bairro').val(data.bairro),
    $('#complemento').val(data.complemento),
    $('#estado').val(data.estado),
    $('#cidade').val(data.cidade),
    $('#telefone').val(data.telefone),
    $('#celular').val(data.celular),
    $('#situacao').val(data.situacao),
    $('#tipoUsuario').val(data.tipoUsuario),
    $('#modalAdd').modal('show')
@endsection
@section('linhaAtualizar')
    linhaAtualizar[0].cells[1].textContent = atualizarItem.nome;
    linhaAtualizar[0].cells[2].textContent = atualizarItem.email;
    linhaAtualizar[0].cells[3].textContent = atualizarItem.cidade;
@endsection